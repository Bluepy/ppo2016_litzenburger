package td3;

import java.io.Serializable;
import java.util.HashMap;

public class Portefeuille implements Serializable
{
	private static final long serialVersionUID = 1902071385017116434L;

	private IllegalArgumentException nomVide = new IllegalArgumentException("Le nom est vide");
	
	private String					nom;
	private HashMap<Devise, Double>	soldes;

	public Portefeuille (String nom)
	{
		this.nom = nom;
		this.soldes = new HashMap<>();
	}

	public String getNom ()
	{
		return nom;
	}

	public void setNom (String nom)
	{
		this.nom = nom;
	}

	public void ajoutDevise (String nomDevise) throws IllegalArgumentException
	{
		if (nomDevise.length() == 0)
			throw nomVide;
		
		if (deviseExiste(nomDevise))
			return;
		
		Devise d;
		
		try {
			d = new Devise(nomDevise);
		}
		catch (IllegalArgumentException e)
		{
			throw nomVide;
		}
		
		soldes.put(d, 0d);
	}

	public void deleteDevise (String nomDevise)
	{
		soldes.remove(new Devise(nomDevise));
	}

	public boolean deviseExiste (String nomDevise) throws IllegalArgumentException
	{
		if (nomDevise.length() == 0)
			throw nomVide;
		
		return soldes.containsKey(new Devise(nomDevise));
	}

	public double getSolde (String nomDevise)
	{
		return soldes.get(new Devise(nomDevise)).doubleValue();
	}

	public void creditSolde (String nomDevise, double credit) throws IllegalArgumentException
	{
		Devise d = new Devise(nomDevise);

		soldes.replace(d, soldes.get(d).doubleValue() + credit);
	}

	public void debitSolde (String nomDevise, double debit) throws OperationNotAuthorizedException
	{
		Devise d = new Devise(nomDevise);
		Double nouveauSolde = soldes.get(d).doubleValue() - debit;

		if (nouveauSolde < 0)
			throw new OperationNotAuthorizedException("Solde insuffisant");

		soldes.replace(d, nouveauSolde);
	}
	
	@Override
	public int hashCode ()
	{
		return this.nom.toLowerCase().hashCode();
	}

	@Override
	public String toString ()
	{
		String _return = "[Portefeuille : '" + nom + "']\n";

		if (soldes.size() == 0)
			_return += "(Aucune devise)";
		else
		{
			for (Devise devise : soldes.keySet())
			{
				_return += "– " + devise.getNom() + " : " + soldes.get(devise).doubleValue() + "\n";
			}
		}

		return _return;
	}
}