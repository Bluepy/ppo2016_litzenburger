package td3;

import td1.Saisie;

public class Client implements Comparable<Client>
{
	private IllegalArgumentException ajoutNegatif = new IllegalArgumentException("Impossible d'ajouter du chiffre d'affaires négatif.");
	
	private String	nom;
	private String	prenom;
	private int	ca;
	
	public Client (String nom, String prenom, int ca)
	{
		this.nom = nom;
		this.prenom = prenom;
		this.ca = ca;
	}

	public Client (String nom, String prenom)
	{
		this.nom = nom;
		this.prenom = prenom;
		this.ca = 0;
	}

	public String getNom ()
	{
		return nom;
	}

	public void setNom (String nom)
	{
		this.nom = nom;
	}

	public String getPrenom ()
	{
		return prenom;
	}

	public void setPrenom (String prenom)
	{
		this.prenom = prenom;
	}

	public int getCA ()
	{
		return ca;
	}

	public void setCA (int ca)
	{
		this.ca = ca;
	}
	
	public Client addCA (int ca)
	{
		if (ca < 0)
			throw ajoutNegatif;
		
		this.ca += ca;

		if (ca >= Privilège.getCaMinPrivilege())
			return new ClientPrivilégié(this);
		else
			return this;
	}

	@Override
	public String toString ()
	{
		return "[Client]\n\tNom : " + this.nom + "\n\tPrénom : " + this.prenom + "\n\tChiffre d'affaires : " + this.ca;
	}

	@Override
	public boolean equals (Object obj)
	{
		if (this == obj)
			return true;

		Client d;

		try
		{
			d = (Client)obj;
		}
		catch (ClassCastException e)
		{
			return false;
		}

		return this.nom.equalsIgnoreCase(d.nom) && this.prenom.equalsIgnoreCase(d.prenom);
	}
	
	@Override
	public int compareTo (Client c)
	{
		return this.compareTo(c);
	}
	
	public Client saisie() {
		String nom = Saisie.saisieChaine("Nom du client :");
		String prénom = Saisie.saisieChaine("Prénom du client :");
		
		return new Client(nom, prénom);
	}
}
