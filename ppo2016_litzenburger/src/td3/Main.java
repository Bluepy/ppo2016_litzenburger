package td3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import td1.Saisie;

public class Main
{
	public static void main (String[] args)
	{
		//ex2();
	}

	public static void ex1 ()
	{
		HashMap<String, Portefeuille> portefeuilles = new HashMap<>();
		int rep;
		
		while (true)
		{
			rep = Saisie.saisieEntierScanner(
				"1. Ajouter portefeuille\n" +
				"2. Gérer portefeuille\n" +
				"———————————————\n"+
				"> "
			);
			
			if (rep == 1)
			{
				String nom = Saisie.saisieChaine("Nom : ");

				if (portefeuilles.containsKey(nom))
				{
					System.err.println("× Le portefeuille '" + nom + "' existe déjà.\n");
					continue;
				}
				
				portefeuilles.put(nom, new Portefeuille(nom));
				
				System.out.println("=> Le portefeuille '" + nom + "' a été créé avec succès.\n");
			}
			else if (rep == 2)
			{
				String nom = Saisie.saisieChaine("Nom : ");
				Portefeuille p = portefeuilles.get(nom);

				if (p == null)
				{
					System.out.println("× Le portefeuille '" + nom + "' n'existe pas.\n");
					continue;
				}
				
				System.out.println(p + "\n");
				
				rep = Saisie.saisieEntierScanner(
					"1. Créditer une devise\n" +
					"2. Débiter une devise\n" +
					"3. Ajouter une devise\n" +
					"4. Retirer une devise\n" +
					"———————————————\n" +
					"> "
				);
				
				if (rep == 1) // crédit
				{
					String nomDevise = Saisie.saisieChaine("Nom : ");

					if (!p.deviseExiste(nomDevise))
					{
						System.out.println("× La devise '" + nomDevise + "' n'existe pas.\n");
						continue;
					}

					double credit = Saisie.saisieReel("Ajouter : ");
					
					p.creditSolde(nomDevise, credit);
					
					System.out.println("=> Crédit effectué. Nouveau solde : " + p.getSolde(nomDevise));
				}
				else if (rep == 2) // débit
				{
					String nomDevise = Saisie.saisieChaine("Nom : ");

					if (!p.deviseExiste(nomDevise))
					{
						System.out.println("× La devise '" + nomDevise + "' n'existe pas.\n");
						continue;
					}

					double debit = Saisie.saisieReel("Ajouter : ");
					
					try
					{
						p.debitSolde(nomDevise, debit);
					}
					catch (OperationNotAuthorizedException e)
					{
						System.out.println("× Débit refusé : " + e.getMessage() + "\n");
					}
					
					System.out.println("=> Débit effectué. Nouveau solde : " + p.getSolde(nomDevise) + "\n");
				}
				else if (rep == 3) // ajout devise
				{
					String nomDevise = Saisie.saisieChaine("Nom : ");

					if (p.deviseExiste(nomDevise))
					{
						System.out.println("La devise '" + nomDevise + "' existe déjà.");
						continue;
					}
					
					try {
						p.ajoutDevise(nomDevise);
					}
					catch (IllegalArgumentException e) {
						System.out.println("× Échec lors de l'ajout de la devise : " + e.getMessage());
					}
					
					System.out.println("=> Devise ajoutée\n");
				}
				else if (rep == 4) // retrait devise
				{
					String nomDevise = Saisie.saisieChaine("Nom : ");

					if (!p.deviseExiste(nomDevise))
					{
						System.out.println("× La devise '" + nomDevise + "' n'existe pas.\n");
						continue;
					}

					p.deleteDevise(nomDevise);
					
					System.out.println("=> Devise supprimée\n");
				}
			}
		}
	}
}