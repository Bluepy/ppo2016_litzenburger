package td3.fichiersplats;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import td1.Saisie;
import td3.OperationNotAuthorizedException;
import td3.Portefeuille;

public class Main
{
	public static void main (String[] args)
	{
		ex2();
	}

	public static void ex2 ()
	{
		HashMap<String, Portefeuille> portefeuilles = new HashMap<>();
		int rep;
		
		while (true)
		{
			rep = Saisie.saisieEntierScanner(
				"1. Ajouter portefeuille\n" +
				"2. Gérer portefeuille\n" +
				"3. Charger portefeuille\n" +
				"4. Sauvegarder portefeuille\n" +
				"———————————————\n"+
				"> "
			);
			
			if (rep == 1)
			{
				String nom = Saisie.saisieChaine("Nom : ");

				if (portefeuilles.containsKey(nom))
				{
					System.err.println("× Le portefeuille '" + nom + "' existe déjà.\n");
					continue;
				}
				
				portefeuilles.put(nom, new Portefeuille(nom));
				
				System.out.println("=> Le portefeuille '" + nom + "' a été créé avec succès.\n");
			}
			else if (rep == 2) // Gérer
			{
				String nom = Saisie.saisieChaine("Nom : ");
				Portefeuille p = portefeuilles.get(nom);

				if (p == null)
				{
					System.out.println("× Le portefeuille '" + nom + "' n'existe pas.\n");
					continue;
				}
				
				while (true)
				{
					System.out.println(p + "\n");
				
					rep = Saisie.saisieEntierScanner(
						"1. Créditer une devise\n" +
						"2. Débiter une devise\n" +
						"3. Ajouter une devise\n" +
						"4. Retirer une devise\n" +
						"0. Retour en arrière\n" +
						"———————————————\n" +
						"> "
					);
				
					if (rep == 1) // crédit
					{
						String nomDevise = Saisie.saisieChaine("Nom : ");
	
						if (!p.deviseExiste(nomDevise))
						{
							System.out.println("× La devise '" + nomDevise + "' n'existe pas.\n");
							continue;
						}
	
						double credit = Saisie.saisieReel("Ajouter : ");
						
						try
						{
							p.creditSolde(nomDevise, credit);
						}
						catch (IllegalArgumentException e)
						{
							System.out.println("× Le nom saisi est vide");
							continue;
						}
						
						System.out.println("=> Crédit effectué. Nouveau solde : " + p.getSolde(nomDevise));
					}
					else if (rep == 2) // débit
					{
						String nomDevise = Saisie.saisieChaine("Nom : ");
	
						if (!p.deviseExiste(nomDevise))
						{
							System.out.println("× La devise '" + nomDevise + "' n'existe pas.\n");
							continue;
						}
	
						double debit = Saisie.saisieReel("Retirer : ");
						
						try
						{
							p.debitSolde(nomDevise, debit);
						}
						catch (OperationNotAuthorizedException e)
						{
							System.out.println("× Débit refusé : " + e.getMessage() + "\n");
							continue;
						}
						catch (IllegalArgumentException e)
						{
							System.out.println("× Le nom saisi est vide");
							continue;
						}
						
						System.out.println("=> Débit effectué. Nouveau solde : " + p.getSolde(nomDevise) + "\n");
					}
					else if (rep == 3) // ajout devise
					{
						String nomDevise = Saisie.saisieChaine("Nom : ");
	
						if (p.deviseExiste(nomDevise))
						{
							System.out.println("La devise '" + nomDevise + "' existe déjà.");
							continue;
						}
						
						try {
							p.ajoutDevise(nomDevise);
						}
						catch (IllegalArgumentException e) {
							System.out.println("× Échec lors de l'ajout de la devise : " + e.getMessage());
						}
						
						System.out.println("=> Devise ajoutée\n");
					}
					else if (rep == 4) // retrait devise
					{
						String nomDevise = Saisie.saisieChaine("Nom : ");
	
						if (!p.deviseExiste(nomDevise))
						{
							System.out.println("× La devise '" + nomDevise + "' n'existe pas.\n");
							continue;
						}
	
						p.deleteDevise(nomDevise);
						
						System.out.println("=> Devise supprimée\n");
					}
					else if (rep == 0) // quitter
					{
						break;
					}
				}
			}
			else if (rep == 3) // Chargement portefeuille
			{
				String nomPortefeuille = Saisie.saisieChaine("Saisir le nom du portefeuille : ");
				Portefeuille p = read(nomPortefeuille);
				
				if (p == null)
					System.out.println("× Erreur lors de la lecture du fichier : il est possible que le format ne soit pas correct.");
					
			
				portefeuilles.put(nomPortefeuille, p);
				
				System.out.println(p);
			}
		}
	}

	public static Portefeuille read (String nomPortefeuille)
	{
		File f = new File(nomPortefeuille + ".txt");
		
		if (!f.exists()) {
			System.out.println("Fichier n'existe pas : " + f.getAbsolutePath());
			return null;
		}
		else
		{
			ArrayList<String> lines = new ArrayList<>();
			Portefeuille p = null;
			
			try
			{
				BufferedReader br = new BufferedReader(new FileReader(f));
				
				if (!br.readLine().equalsIgnoreCase("PORTEFEUILLE"))
				{
					br.close();
					return null;
				}
				
				p = new Portefeuille(nomPortefeuille);
				String line;
				while ( (line = br.readLine().trim()) != null )
				{
					if (line.equalsIgnoreCase("FIN PORTEFEUILLE")) break;
					
					String[] deviseData = line.split(":");
					String nomDevise = deviseData[0].trim();
					double credit = Double.parseDouble(deviseData[1].trim());
					
					p.ajoutDevise(nomDevise);
					p.creditSolde(nomDevise, credit);
				}
				
				br.close();
			}
			catch (IOException | NumberFormatException e)
			{
				System.out.println(e.getLocalizedMessage());
				return null;
			}
			
			return p;
		}
	}
}