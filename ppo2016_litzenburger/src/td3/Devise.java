package td3;

import java.io.Serializable;

public class Devise implements Comparable<Devise>, Serializable
{
	private static final long serialVersionUID = 3523773590427733533L;

	private String nom;
	
	private static IllegalArgumentException
		nomVideException = new IllegalArgumentException("Le nom de devise est vide.");

	public Devise (String nom) throws IllegalArgumentException
	{
		String nomDevise = nom.trim();
		
		if (nomDevise.length() == 0)
			throw nomVideException;
		
		this.nom = formatNomDevise(nom);
	}
	
	/**
	 * Formatte un nom de devise pour recherche dans une liste de devises
	 * @param nomDevise un nom de devise
	 * @return le nom au format correct
	 */
	public static String formatNomDevise (String nomDevise)
	{
		return Character.toUpperCase(nomDevise.charAt(0)) + nomDevise.substring(1).toLowerCase();
	}

	public String getNom ()
	{
		return nom;
	}

	public void setNom (String nom)
	{
		String nomDevise = nom.trim();
		
		if (nomDevise.length() == 0)
			throw nomVideException;
		
		this.nom = nomDevise;
	}

	@Override
	public String toString ()
	{
		return "";
	}

	@Override
	public int hashCode ()
	{
		return this.nom.toUpperCase().hashCode();
	}

	@Override
	public boolean equals (Object obj)
	{
		if (this == obj)
			return true;

		Devise d;

		try
		{
			d = (Devise)obj;
		}
		catch (ClassCastException e)
		{
			return false;
		}

		return d.nom.equalsIgnoreCase(this.nom);
	}

	@Override
	public int compareTo (Devise d)
	{
		return this.nom.compareTo(d.nom);
	}
}