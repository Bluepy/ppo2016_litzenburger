package td1;

public class TabIndex {
	int[] tab;
	int index;
	
	public TabIndex(int[] tab, int index) {
		this.tab = tab;
		this.index = index;
	}
	
	@Override
	public String toString() {
		return Bonjour.afficheTable(tab) + "————————————————————\nIndex : " + index;
	}
}
