package td1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.InputMismatchException;
import java.util.Scanner;

/*
 * Exercice 8 : Class Saisie et méthode de saisie d'une chaîne
 */
public class Saisie {
	public static int saisieEntierScanner() {
		return saisieEntierScanner(null);
	}
	
	/*
	 * Ex. 9 : surcharge des méthodes
	 */
	@SuppressWarnings("resource")
	public static int saisieEntierScanner(String msg) {
		if (msg != null)
			System.out.print(msg);
		
		Scanner sc = new Scanner(System.in);
		
		int entier = -1;
		
		while (true) {
			try {
				entier = Integer.parseInt(sc.nextLine());
			}
			catch (NumberFormatException e) {
				if (msg != null)
					System.out.print("Erreur de saisie. La valeur doit être un entier. Resaisir : ");
				
				continue;
			}
			
			break;
		}
		
		return entier;
	}
	
	public static int saisieEntierBufferedReader() {
		return saisieEntierBufferedReader(null);
	}
	
	/*
	 * Ex. 9 : surcharge des méthodes
	 */
	public static int saisieEntierBufferedReader(String msg) {
		if (msg != null)
			System.out.print(msg);
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int entier = -1;
		
		while (true) {
			try {
				entier = Integer.parseInt(br.readLine());
			}
			catch (NumberFormatException | IOException e) {
				if (msg != null)
					System.out.print("Erreur de saisie. La valeur doit être un entier. Resaisir : ");
				
				continue;
			}
			
			break;
		}
		
		return entier;
	}

	public static double saisieReel() {
		return saisieReel(null);
	}
	
	/*
	 * Ex. 9 : surcharge des méthodes
	 */
	@SuppressWarnings("resource")
	public static double saisieReel(String msg) {
		if (msg != null)
			System.out.print(msg);
		
		Scanner sc = new Scanner(System.in);
		
		double reel = -1;
		
		while (true) {
			try {
				reel = sc.nextDouble();
			}
			catch (InputMismatchException e) {
				if (msg != null)
					System.out.print("Erreur de saisie. La valeur doit être un réel. Resaisir : ");
				
				continue;
			}
			
			break;
		}
		
		return reel;
	}
	
	public static String saisieChaine() {
		return saisieChaine(null);
	}
	
	/*
	 * Ex. 9 : surcharge des méthodes
	 */
	public static String saisieChaine(String msg) {
		return saisieChaine(msg, null, null);
	}

	@SuppressWarnings("resource")
	public static String saisieChaine(String msg, String msgError, String regex) {
		if (msg != null)
			System.out.print(msg);
		
		Scanner sc = new Scanner(System.in);
		
		String chaine = null;

		chaine = sc.nextLine();
		
		if (regex != null) {
			while (!chaine.matches(regex)) {
				System.out.print(msgError + " ");
				chaine = sc.nextLine();
			}
		}
		
		return chaine;
	}
}