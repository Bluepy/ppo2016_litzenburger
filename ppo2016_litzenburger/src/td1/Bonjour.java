package td1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Scanner;

/*
 * Exercice 1 :
 * 	Création de la classe et de son constructeur
 */
public class Bonjour {
	private String nom, prenom;
	private int age;
	
	public Bonjour() {}
	
	public Bonjour (String nom, String prenom, int age) {
		this.nom = nom;
		this.prenom = prenom;
		this.age = age;
	}
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String toString() {
		return "Bonjour " + prenom + " " + nom + ", tu as " + age + " ans.";
	}
	
	/*
	 * Exercice 2 :
	 * 	Gestion de la saisie
	 * 	Affichage dans la console
	 */
//	@SuppressWarnings("resource")
	public void saisieNomPrenom() {
//		Scanner sc = new Scanner(System.in);
//		
//		System.out.println("Saisir votre prénom :");
//		String prenom = sc.nextLine();
		
		/*
		 * Exercice 3 :
		 * 	Formattage prénom
		 */
//		prenom = prenom.substring(0, 1).toUpperCase() + prenom.substring(1);
		
		/*
		 * Exercice 4 :
		 * 	Vérification du prénom à l'aide d'une regex et boucle tant que le prénom n'est pas valide
		 * 	Exemples de prénoms valides :
		 * 		- Guillaume
		 * 		- Jean-Luc
		 * 		- Pierre-Paul-Jacques
		 * 
		 * 	Exemples de prénoms invalides :
		 * 		- Bob--
		 * 		- -non-
		 * 		- minuscule
		 * 		- ROGER
		 */
//		while (!prenom.matches("^([A-Z][a-z]+)(-[A-Z][a-z]+)*$")) {
//			System.out.println("Le prénom est incorrect ! La première lettre doit être en majuscule, les autres en minuscules.");
//			prenom = sc.nextLine();
//		}
		/* ---------------------------- */
		
//		System.out.println("Saisir votre nom :\n>");
//		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
//		
//		String nom = null;
//		try {
//			nom = br.readLine();
//		}
//		catch (IOException e) {
//			e.printStackTrace();
//			System.exit(1);
//		}
		
		/*
		 * Exercice 3 :
		 * 	Formattage nom
		 */
//		nom = nom.toUpperCase();
//		
//		if (nom == null) {
//			System.out.println("Erreur readLine !");
//			System.exit(1);
//		}
		
		/*
		 * Exercice 4 :
		 * 	Vérification du nom à l'aide d'une regex et boucle tant que le nom n'est pas valide
		 * 	
		 * 	Exemples de noms valides :
		 * 		- LITZENBURGER
		 * 		- DE-BERE
		 */
//		while (!nom.matches("^([A-Z]+)(-[A-Z]+)*$")) {
//			System.out.println("Le nom est incorrect ! Il doit être tout en majuscules.");
//			
//			try {
//				nom = br.readLine();
//			}
//			catch (IOException e) {
//				e.printStackTrace();
//				System.exit(1);
//			}
//		}
		/* ------------------------------------ */
		
//		this.nom = nom;
//		this.prenom = prenom;
		
		this.saisieNom();
		this.saisiePrenom();
	}
	
	/*
	 * Exercice 5 : saisie entier
	 */
	public static int saisieEntierBufferedReader() {
		System.out.println("Saisir un entier :");
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int entier = -1;
		
		/*
		 * Exercice 6 : try/catch et boucle
		 */
		while (true) {
			try {
				entier = Integer.parseInt(br.readLine());
			}
			catch (NumberFormatException | IOException e) {
				System.out.println("Valeur incorrecte ! Resaisir :");
				continue;
			}
			
			break;
		}
		
		return entier;
	}
	
	/*
	 * Exercice 5 : saisie entier
	 */
	@SuppressWarnings("resource")
	public static int saisieEntierScanner() {
		System.out.println("Saisir un entier :");
		
		Scanner sc = new Scanner(System.in);
		
		int entier = -1;
		
		/*
		 * Exercice 6 : try/catch et boucle
		 */
		while (true) {
			try {
				entier = Integer.parseInt(sc.nextLine());
			}
			catch (NumberFormatException e) {
				System.out.println("Valeur incorrecte ! Resaisir :");
				continue;
			}
			
			break;
		}
		
		return entier;
	}
	
	/*
	 * Exercice 7 : méthode de saisie d'un réel
	 */
	@SuppressWarnings("resource")
	public float saisieReel() {
		Scanner sc = new Scanner(System.in);
		
		float reel = -1;
		
		/*
		 * Exercice 6 : try/catch et boucle
		 */
		while (true) {
			try {
				reel = Float.parseFloat(sc.nextLine());
			}
			catch (NumberFormatException e) {
				System.out.println("Valeur incorrecte ! Resaisir :");
				continue;
			}
			
			break;
		}
		
		return reel;
	}
	
	public void saisieNom() {
		this.nom = Saisie.saisieChaine("Saisir votre nom :", "Le nom est incorrect ! Il doit être tout en majuscules.", "^([A-Z]+)(-[A-Z]+)*$");
	}
	
	public void saisiePrenom() {
		this.prenom = Saisie.saisieChaine("Saisir votre prénom :", "Le prénom est incorrect ! La première lettre doit être en majuscule, les autres en minuscules.", "^([A-Z][a-z]+)(-[A-Z][a-z]+)*$");
	}
	
	public void saisieAnneeNaissance() {
//		int anneeAujourdhui = LocalDate.now().getYear();
		LocalDate now = LocalDate.now();
		LocalDate dateNaissance = now.withYear(Saisie.saisieEntierScanner("Saisir votre année de naissance :"));
		
//		this.age = anneeAujourdhui - Saisie.saisieEntierScanner("Saisir votre date de naissance :");
		
		/*
		 * Exercice 10 : calcul en fonction de la date d'aujourd'hui
		 */
		int age = Period.between(dateNaissance, now).getYears();
		System.out.println("Date naissance : " + dateNaissance.format(DateTimeFormatter.ofPattern("eeee d MMMM u")) + " —— Âge : " + age);
		this.age = age;
	}

	public static void affichageDate(int year) {
		LocalDate ld = LocalDate.now().withYear(year);
		System.out.println(ld.format(DateTimeFormatter.ofPattern("d/M/y")));
		System.out.println(ld.format(DateTimeFormatter.ofPattern("eeee d MMMM u")));
	}
	
	/*
	 * Ex 11
	 */
	public static int[] saisieTable() {
		int tableSize = Saisie.saisieEntierScanner("Saisir la taille de la table :");
		int[] table = new int[tableSize];
		
		for (int i = 0; i < tableSize; i++)
			table[i] = Saisie.saisieEntierScanner(i + ":");
		
		return table;
	}
	
	public static String afficheTable (int[] tab) {
		String text = "";
		int length = tab.length;
		text += "Table :\n[\n";
		
		for (int i = 0; i < length; i++) {
			text += "\t" + i + ": " + tab[i];
			
			if (i != length - 1)
				text += ",";
			
			text += "\n";
		}
		
		text += "]\n";
		
		System.out.println(text);
		return "";
	}
	
	/*
	 * Ex 12
	 */
	public static int[] copieTable (int[] tab) {
		int length = tab.length;
		int[] clone = new int[length];
		
		for (int i = 0; i < length; i++)
			clone[i] = tab[i];
		
		return clone;
	}
	
	public static int[] triBulle(int[] tab) {
		for (int i = tab.length - 1; i >= 1; i--) {
			for (int j = 0; j < i - 1; j++) {
				if (tab[j + 1] < tab[j]) {
					int temp = tab[j];
					
					tab[j] = tab[j + 1];
					tab[j + 1] = temp;
				}
			}
		}
		return tab;
	}
	
	public static int indexOf(int[] tab, int value) {
		int i;
		Boolean found = true;
		
		for (i = 0; tab[i] != value; i++) {
			if (i + 1 == tab.length) { // valeur suivante est fin du tableau
				found = false;
				break;
			}
		}
		
		if (found)
			return i;
		else
			return -1;
	}
	
	public static TabIndex copieTriIndex(int[] tab, int value) {
		int length = tab.length;
		int[] copie = new int[length];
		copie = Arrays.copyOf(tab, length);
		
		Arrays.sort(copie);
				
		return new TabIndex(copie, Arrays.binarySearch(copie, value));
	}
}