package td2;

import java.util.ArrayList;
import java.util.Comparator;

public class Clientèle
{
	private ArrayList<Client> clientèle;

	public Clientèle ()
	{
		clientèle = new ArrayList<>();
	}

	public boolean add (Client c)
	{
		if (clientèle.contains(c))
			return false;

		clientèle.add(c);
		return true;
	}
	
	public boolean delete (Client c) {
		return clientèle.remove(c);
	}
	
	public Client getClient (int num) {
		return clientèle.get(num);
	}
	
	public Client setClient (int num, Client c) {
		return clientèle.set(num, c);
	}
	
	public ArrayList<Client> getClientele ()
	{
		return clientèle;
	}

	public void addCA (int num, int chiffre) throws IndexOutOfBoundsException
	{
		Client c = this.clientèle.get(num);

		c.addCA(chiffre);
	}

	public int indexOf (Client c)
	{
		return this.clientèle.indexOf(c);
	}

	public ArrayList<Client> sort ()
	{
		clientèle.sort(new Comparator<Client>() {
			@Override
			public int compare (Client cl1, Client cl2)
			{
				int _return = 0;
				
				if (cl1.getCA() > cl2.getCA())
					_return = -1;
				else if (cl1.getCA() < cl2.getCA())
					_return = 1;
				
				return _return;
			}
		});
		
		return clientèle;
	}
	
	@Override
	public String toString ()
	{
		return clientèle.toString();
	}
	
	public void affiche() {
		System.out.println();
	}
}