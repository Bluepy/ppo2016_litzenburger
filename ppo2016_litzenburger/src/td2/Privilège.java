package td2;

public enum Privilège
{
	BON_CLIENT(1000, 5), CLIENT_EXCEPTIONNEL(3000, 15), VIC(10000, 3);

	private int	caMin;
	private int	reduc;

	Privilège (int caMin, int reduc)
	{
		this.caMin = caMin;
		this.reduc = reduc;
	}

	public int getCaMin ()
	{
		return caMin;
	}

	public int getReduc ()
	{
		return reduc;
	}
	
	public static int getCaMinPrivilege() {
		Privilège[] privilèges = Privilège.values();
		Privilège privilègeMin = privilèges[0];
		
		for (Privilège privilège : privilèges) {
			if (privilège.getCaMin() < privilègeMin.getCaMin())
				privilègeMin = privilège;
		}
		
		return privilègeMin.getCaMin();
	}
	
	@Override
	public String toString ()
	{
		return this.name();
	}
}
