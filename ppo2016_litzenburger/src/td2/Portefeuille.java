package td2;

import java.util.ArrayList;
import java.util.HashMap;

public class Portefeuille
{
	private ArrayList<Devise>		devises;
	private HashMap<Devise, Double>	soldes;

	public Portefeuille ()
	{
		this.devises = new ArrayList<>();
		this.soldes = new HashMap<>();
	}

	public void ajoutDevise (Devise devise) throws IllegalArgumentException
	{
		if (this.devises.contains(devise))
		{
			Devise d = this.devises.get(this.devises.indexOf(devise));

			d.credit(devise.getSolde());
		}

		this.devises.add(devise);
		this.soldes.put(devise, 0.0);
	}
	
	public void deleteDevise (String devise) throws IndexOutOfBoundsException {
		if (!this.devises.remove(getDevise(devise)))
			throw new IndexOutOfBoundsException("La devise \"" + devise + "\" n'existe pas.");
	}
	
	public Devise getDevise(String devise) throws IndexOutOfBoundsException {
		try
		{
			return this.devises.get(this.devises.indexOf(Devise.formatNomDevise(devise)));
		}
		catch (IndexOutOfBoundsException e)
		{
			throw new IndexOutOfBoundsException("La devise \"" + devise + "\" n'existe pas.");
		}
	}

	public float getSolde (Devise devise)
	{
		return devise.getSolde();

		// return soldes.get(devise).floatValue();
	}

	public ArrayList<Devise> getListeDevise ()
	{
		return devises;
	}

	public void creditSolde (String devise, double credit) throws IndexOutOfBoundsException, IllegalArgumentException
	{
		Devise d = getDevise(devise);
		
		d.credit(credit);

		/*
		 * Après
		 */
		// float soldeActuel = getSolde(devise);
		//
		// soldes.replace(devise, soldeActuel + credit);
		//
		// return true;
	}

	public void debitSolde (String devise, double debit) throws IndexOutOfBoundsException
	{
		Devise d = this.devises.get(this.devises.indexOf(new Devise(devise)));
		
		d.debit(debit);

		/*
		 * Après
		 */
		// float soldeActuel = getSolde(devise);
		//
		// if ((soldeActuel - debit) < 0)
		// return false;
		//
		// soldes.replace(devise, soldeActuel - debit);

		// return true;
	}
}