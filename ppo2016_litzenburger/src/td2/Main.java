package td2;

import java.util.ArrayList;

import td1.Saisie;

public class Main
{
	public static void main (String[] args)
	{
		ex2();
	}

	public static void ex1 ()
	{
		Portefeuille pf = new Portefeuille();

		Devise euro = new Devise("Euro", 1);
		Devise lol = new Devise("lol", 2);

		pf.ajoutDevise(euro);
		pf.ajoutDevise(lol);

		pf.creditSolde("Euro", 100);
		pf.creditSolde("lol", 2);

		System.out.println("Solde d'euros : " + pf.getSolde(euro));

		try
		{
			pf.debitSolde("euros", 60);
		}
		catch (IndexOutOfBoundsException e)
		{
			System.out.println("Devise non trouvée");
		}

		System.out.println("Solde après débit : " + pf.getSolde(euro));

		// if (!pf.debitSolde("lol", 1.2))
		System.out.println("Le solde de lol n'est pas suffisant. Son solde est de " + pf.getSolde(lol)
				+ " alors qu'on essaie de retirer 5.");
		// else
		System.out.println("Solde de lol après débit : " + pf.getSolde(lol));
	}

	public static void ex2 ()
	{
		String format = "%15s | %15s | %12s";
		
		Portefeuille pf = new Portefeuille();
		Clientèle clientèle = new Clientèle();
		String rep;

		while (true)
		{
			rep = Saisie.saisieChaine("Ajouter un client dans la clientèle : add\nSupprimer un client : del\nModifier un client : modif\nAfficher la liste des clients : list\nGérer votre portefeuille : pf\n> ");

			if (rep.equalsIgnoreCase("add"))
			{
				rep = Saisie.saisieChaine("Saisir dans l'ordre : nom prénom");

				String[] nomPrénom = rep.split(" ");

				if (clientèle.add(new Client(nomPrénom[0], nomPrénom[1]))) {
					System.out.println("Le client \"" + rep + "\" a été ajouté avec succès");
				}
				else
					System.err.println("Le client \"" + rep + "\" existe déjà.");
			}
			else if (rep.equalsIgnoreCase("del"))
			{
				if (clientèle.getClientele().size() == 0) {
					System.err.println("Impossible de supprimer un client dans la clientèle lorsque celle-ci est vide.");
					continue;
				}
				
				rep = Saisie.saisieChaine("Saisir dans l'ordre : nom prénom");

				String[] nomPrénom = rep.split(" ");

				if (clientèle.delete(new Client(nomPrénom[0], nomPrénom[1]))) {
					System.out.println("Le client \"" + rep + "\" a été supprimé avec succès.");
				}
				else {
					System.err.println("Le client \"" + rep + "\" n'existe pas dans la clientèle.");
				}
			}
			else if (rep.equalsIgnoreCase("modif"))
			{
				if (clientèle.getClientele().size() == 0) {
					System.err.println("Impossible de modifier un client dans la clientèle lorsque celle-ci est vide.");
					continue;
				}
				
				rep = Saisie.saisieChaine("Saisir dans l'ordre : nom prénom");

				String[] nomPrénom = rep.split(" ");

				int numClient = clientèle.indexOf(new Client(nomPrénom[0], nomPrénom[1]));
				
				if (numClient < 0)
				{
					System.err.println("Le client \"" + rep + "\" n'existe pas dans la clientèle.");
					continue;
				}
				
				// On obtient l'instance du client recherché dans la clientèle.
				Client c = clientèle.getClient(numClient);

				rep = Saisie.saisieChaine("Modifier : nom, prénom ou CA (ajout seulement) ?");
				
				if (rep.equalsIgnoreCase("nom")) {
					String nom = Saisie.saisieChaine("Saisir le nouveau nom (actuel : " + c.getNom() + ") : ");
					c.setNom(nom);
				}
				else if (rep.equalsIgnoreCase("prénom")) {
					String prenom = Saisie.saisieChaine("Saisir le nouveau prénom (actuel : " + c.getPrenom() + ") : ");
					c.setNom(prenom);
				}
				else if (rep.equalsIgnoreCase("ca")) {
					int ca = Saisie.saisieEntierScanner("Chiffre d'affaire a ajouter ? (actuel : " + c.getCA() + ") : ");
					c = c.addCA(ca);
					
					if (c instanceof ClientPrivilégié) {
						System.out.println("Le client \"" + c.getNom() + " " + c.getPrenom() + "\" est un client privilégié !");
						clientèle.setClient(numClient, c);
					}
				}
				else
					continue;
				
				
			}
			else if (rep.equalsIgnoreCase("list")) {
				ArrayList<Client> clients = clientèle.getClientele();
				
				if (clients.size() < 1) {
					System.out.println("Aucun client enregistré.");
					continue;
				}
				
				System.out.println("\n" + String.format(format, "Nom", "Prénom", "CA"));
				
				for (Client client : clients) {
					String privilégié = "";
					if (client instanceof ClientPrivilégié) privilégié = "* ";

					System.out.println(String.format(format, privilégié + client.getNom(), client.getPrenom(), client.getCA()));
				}
				
				System.out.println();
			}
			else if (rep.equalsIgnoreCase("pf")) {
				if (pf.getListeDevise().size() == 0) {
					System.err.println("Votre portefeuille est vide.");
				}
				else {
					ArrayList<Devise> devises = pf.getListeDevise();
					
					System.out.println("\nNom [tx change] : solde");
					for (Devise devise : devises) {
						System.out.println(devise.getNom() + " [" + devise.getTauxChangeEuro() + "] : " + devise.getSolde());
					}
				}

				rep = Saisie.saisieChaine("Ajouter une devise : add\nSupprimer une devise : suppr\n>");
				
				if (rep.equalsIgnoreCase("add")) {
					String nom = Saisie.saisieChaine("Saisir le nom de la devise : ");
					double tx = Saisie.saisieReel("Saisir le taux de change : ");
					
					pf.ajoutDevise(new Devise(nom, tx));
				}
				else if (rep.equalsIgnoreCase("suppr")) {
					String nom = Saisie.saisieChaine("Saisir le nom de la devise : ");
					
					try
					{
						pf.deleteDevise(nom);
					}
					catch (IndexOutOfBoundsException e)
					{
						System.err.println(e.getMessage());
						continue;
					}
					
					System.out.println("Devise supprimée avec succès.");
				}
				
				else if (rep.equalsIgnoreCase("modif")) {
					continue;
				}
			}
			else if (rep.equalsIgnoreCase("exit")) {
				System.out.println("Au revoir !");
				break;
			}
		}
	}
}