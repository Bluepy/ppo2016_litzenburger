package td2;

public class Devise implements Comparable<Devise>
{
	private String	nom;
	private double	tauxChangeEuro;
	private float	solde;
	
	private static IllegalArgumentException
		nomVideException = new IllegalArgumentException("Le nom de devise est vide."),
		tauxChangeInvalideException = new IllegalArgumentException("Le taux de change est invalide."),
		soldeNulOuNegatifException = new IllegalArgumentException("Le solde ne peut pas être nul."),
		ajoutNegatif = new IllegalArgumentException("Impossible d'ajouter un montant négatif.");

	public Devise (String nom)
	{
		this.nom = nom;
	}

	public Devise (String nom, double tauxChangeEuro)
	{
		String nomDevise = nom.trim();
		
		if (nomDevise.length() == 0)
			throw nomVideException;
		
		if (tauxChangeEuro <= 0)
			throw tauxChangeInvalideException;
		
		if (solde < 0)
			throw soldeNulOuNegatifException;
		
		this.nom = formatNomDevise(nomDevise);
		this.tauxChangeEuro = tauxChangeEuro;
		this.solde = 0;
	}
	
	/**
	 * Formatte un nom de devise pour recherche dans une liste de devises
	 * @param nomDevise un nom de devise
	 * @return le nom au format correct
	 */
	public static String formatNomDevise (String nomDevise)
	{
		return Character.toUpperCase(nomDevise.charAt(0)) + nomDevise.substring(1);
	}

	public String getNom ()
	{
		return nom;
	}

	public void setNom (String nom)
	{
		String nomDevise = nom.trim();
		
		if (nomDevise.length() == 0)
			throw nomVideException;
		
		this.nom = nomDevise;
	}

	public double getTauxChangeEuro ()
	{
		return tauxChangeEuro;
	}

	public void setTauxChangeEuro (double tauxChangeEuro)
	{
		this.tauxChangeEuro = tauxChangeEuro;
	}

	public float getSolde ()
	{
		return solde;
	}

	public void credit (double credit)
	{
		if (credit < 0)
			throw ajoutNegatif;

		this.solde += credit;
	}

	public void debit (double d)
	{
//		if (this.solde - d < 0)
//			throw new 

		this.solde -= d;
	}

	@Override
	public String toString ()
	{
		return "Devise [nom=" + nom + ", tauxChangeEuro=" + tauxChangeEuro + "]";
	}

	@Override
	public int hashCode ()
	{
		return nom.hashCode();
	}

	@Override
	public boolean equals (Object obj)
	{
		if (this == obj)
			return true;

		Devise d;

		try
		{
			d = (Devise)obj;
		}
		catch (ClassCastException e)
		{
			return false;
		}

		return d.nom.equalsIgnoreCase(this.nom);
	}

	@Override
	public int compareTo (Devise d)
	{
		return this.nom.compareTo(d.nom);
	}
}