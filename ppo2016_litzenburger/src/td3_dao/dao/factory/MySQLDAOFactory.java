package td3_dao.dao.factory;

import td3_dao.dao.ContenuDAO;
import td3_dao.dao.DeviseDAO;
import td3_dao.dao.PortefeuilleDAO;
import td3_dao.dao.mysql.MySQLContenuDAO;
import td3_dao.dao.mysql.MySQLDeviseDAO;
import td3_dao.dao.mysql.MySQLPortefeuilleDAO;

public class MySQLDAOFactory extends DAOFactory
{
	@Override
	public ContenuDAO getContenuDAO ()
	{
		return MySQLContenuDAO.getInstance();
	}

	@Override
	public DeviseDAO getDeviseDAO ()
	{
		return MySQLDeviseDAO.getInstance();
	}
	
	@Override
	public PortefeuilleDAO getPortefeuilleDAO ()
	{
		return MySQLPortefeuilleDAO.getInstance();
	}
}