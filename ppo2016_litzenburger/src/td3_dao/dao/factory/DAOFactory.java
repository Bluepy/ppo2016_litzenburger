package td3_dao.dao.factory;

import td3_dao.dao.ContenuDAO;
import td3_dao.dao.DeviseDAO;
import td3_dao.dao.PortefeuilleDAO;

public abstract class DAOFactory
{
	public static DAOFactory getDAOFactory (Persistance cible)
	{
		DAOFactory daoF = null;
		switch (cible)
		{
			case MYSQL:
				daoF = new MySQLDAOFactory();
				break;
		}
		return daoF;
	}

	public abstract ContenuDAO getContenuDAO();
	public abstract DeviseDAO getDeviseDAO();
	public abstract PortefeuilleDAO getPortefeuilleDAO();
}