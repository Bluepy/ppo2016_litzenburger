package td3_dao.dao;

import td3_dao.modele.metier.Portefeuille;

public interface PortefeuilleDAO extends DAO<Portefeuille>
{
	public int getID (String nom) throws RuntimeException;
	public Portefeuille getById (int id) throws RuntimeException;
	public Portefeuille getByNom (String nom) throws RuntimeException;
}