package td3_dao.dao;

public interface DAO<T>
{
	public void create (T obj);
	public void update (T obj);
	public void delete (T obj);
}