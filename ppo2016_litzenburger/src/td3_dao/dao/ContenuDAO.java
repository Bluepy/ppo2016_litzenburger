package td3_dao.dao;

import java.util.HashMap;

import td3_dao.modele.metier.Devise;
import td3_dao.modele.metier.Portefeuille;

public interface ContenuDAO extends DAO<Portefeuille>
{
	public HashMap<Devise, Double> getByPortefeuille (int id);
}
