package td3_dao.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import td3_dao.dao.PortefeuilleDAO;
import td3_dao.exceptions.AlreadyExistsException;
import td3_dao.modele.metier.Portefeuille;

public class MySQLPortefeuilleDAO implements PortefeuilleDAO
{
	private static MySQLPortefeuilleDAO instance;

	private MySQLPortefeuilleDAO() {}
	
	public static MySQLPortefeuilleDAO getInstance()
	{
		if (instance == null)
			instance = new MySQLPortefeuilleDAO();
		
		return instance;
	}

	@Override
	public void create (Portefeuille obj) throws RuntimeException
	{
		if (obj == null)
			throw new NullPointerException("Obj parameter is null in create method");
		
		
		Connection connexion = ConnexionMySQL.getInstance().getConnection();
		int idPortefeuille = -1;
		
		// Portefeuille en lui-même
		try
		{
			PreparedStatement requete = connexion.prepareStatement("INSERT INTO Portefeuille (nom_port) VALUES (?)", PreparedStatement.RETURN_GENERATED_KEYS);
			
			requete.setString(1, obj.getNom());
			int result = requete.executeUpdate();
			
			if (result == 0)
				throw new RuntimeException("Can not create");
			
			
			ResultSet generatedKeys = requete.getGeneratedKeys();
			
			if (!generatedKeys.next())
				throw new SQLException("ResultSet de insert dans MySQLPortefeuilleDAO.create est vide");
				
			idPortefeuille = generatedKeys.getInt(1);
			
			if (idPortefeuille == 0)
				throw new SQLException("ID retourné par requête insert dans MySQLPortefeuilleDAO.create est nul");
		}
		catch (SQLException excep)
		{
			if (excep.getErrorCode() == 1062)
				throw new AlreadyExistsException("Le portefeuille \"" + obj.getNom() + "\" existe déjà dans la base de données.");
			
			excep.printStackTrace();
			System.exit(1);
		}
		
		// Le contenu du portefeuille
		MySQLContenuDAO contenuDAO = MySQLContenuDAO.getInstance();
		obj.setId(idPortefeuille);
		contenuDAO.create(obj);
	}

	@Override
	public void update (Portefeuille obj)
	{
		if (obj == null)
			throw new NullPointerException("Obj parameter is null in update method");
		
		
		Connection connexion = ConnexionMySQL.getInstance().getConnection();
		
		try
		{
			//Nom
			PreparedStatement updatePortefeuille = connexion.prepareStatement("UPDATE Portefeuille SET nom_port = ? WHERE id_port = ?");
			updatePortefeuille.setString(1, obj.getNom());
			updatePortefeuille.setInt(2, obj.getId());
		}
		catch (SQLException e)
		{
			// TODO: handle exception
		}
	}

	@Override
	public void delete (Portefeuille obj)
	{
		// TODO Auto-generated method stub
		
	}
	
	public int getID (String nom)
	{
		Connection connexion = ConnexionMySQL.getInstance().getConnection();
		 
		try
		{
			PreparedStatement requete = connexion.prepareStatement("SELECT id_port FROM portefeuille WHERE nom_port = ?");
			requete.setString(1, nom);
			ResultSet result = requete.executeQuery();
			
			if (result.next())
				return result.getInt(1);
			else
				return -1;
		}
		catch (SQLException excep)
		{
			System.out.println("Error : " + excep.getMessage());
			System.exit(1);
		}
		
		return -1;
	}
	
	@Override
	public Portefeuille getById (int id)
	{
		Connection connexion = ConnexionMySQL.getInstance().getConnection();
		Portefeuille p = null;
		
		try
		{
			// Nom du portefeuille
			PreparedStatement nomPortefeuille = connexion.prepareStatement("SELECT nom_port FROM portefeuille WHERE id_port = ?");
			nomPortefeuille.setInt(1, id);
			ResultSet resultNomPortefeuille = nomPortefeuille.executeQuery();
			
			if (!resultNomPortefeuille.next())
				return p;
			
			String nom = resultNomPortefeuille.getString(1);
			p = new Portefeuille(nom);
			
			// Le contenu
			PreparedStatement contenuPortefeuille = connexion.prepareStatement(
					"SELECT d.id_dev, nom_dev, montant " +
					"FROM Contenu c " +
					"INNER JOIN Devise d ON c.id_dev = d.id_dev " +
					"WHERE id_port = ?");
			contenuPortefeuille.setInt(1, id);
			ResultSet resultContenuPortefeuille = contenuPortefeuille.executeQuery();
			
			if (!resultContenuPortefeuille.next())
				return p;
			
			do
			{
				String nomDevise = resultContenuPortefeuille.getString(2);
				double credit = resultContenuPortefeuille.getDouble(3);
				p.ajoutDevise(nomDevise);
				p.creditSolde(nomDevise, credit);
			}
			while (resultContenuPortefeuille.next());
		}
		catch (SQLException excep)
		{
			excep.printStackTrace();
		}
		
		return p;
	}

	@Override
	public Portefeuille getByNom (String nom)
	{
		Connection connexion = ConnexionMySQL.getInstance().getConnection();
		Portefeuille p = null;
		
		try
		{
			PreparedStatement getByNom = connexion.prepareStatement("SELECT id_port FROM portefeuille WHERE nom_port = ?");
			getByNom.setString(1, nom);
			ResultSet result = getByNom.executeQuery();
			
			if (!result.next())
				return null;
			
			int id = result.getInt(1);
			p = getById(id);
			
			if (p == null)
				return null;
				
			p.setId(id);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		
		return p;
	}
}