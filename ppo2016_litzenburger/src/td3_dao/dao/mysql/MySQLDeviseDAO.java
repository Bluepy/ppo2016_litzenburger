package td3_dao.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import td3_dao.dao.DeviseDAO;
import td3_dao.modele.metier.Devise;

public class MySQLDeviseDAO implements DeviseDAO
{

	private static MySQLDeviseDAO instance;

	private MySQLDeviseDAO() {}
	
	public static MySQLDeviseDAO getInstance()
	{
		if (instance == null)
			instance = new MySQLDeviseDAO();
		
		return instance;
	}
	
	@Override
	public void create (Devise obj)
	{
		if (obj == null)
			throw new NullPointerException("Obj parameter is null in create method");
		
		
		Connection connexion = ConnexionMySQL.getInstance().getConnection();
		
		try
		{
			PreparedStatement insertDevise = connexion.prepareStatement("INSERT INTO Devise (nom_dev) VALUES (?)", PreparedStatement.RETURN_GENERATED_KEYS);
			insertDevise.setString(1, obj.getNom());
			
			insertDevise.executeUpdate();
			
			ResultSet generatedKeys = insertDevise.getGeneratedKeys();
			if (!generatedKeys.next())
				throw new SQLException("ResultSet de insert dans MySQLDeviseDAO.create est vide");
			
			int idDevise = generatedKeys.getInt(1);
			
			if (idDevise == 0)
				throw new SQLException("ID retourné par requête insert dans MySQLDeviseDAO.create est nul");
			
			
			obj.setId(idDevise);
		}
		catch (SQLException excep)
		{
			System.err.println("Error : " + excep.getMessage());
			excep.printStackTrace();
			System.exit(1);
		}
	}

	@Override
	public void update (Devise obj)
	{
		if (obj == null)
			throw new NullPointerException("Obj parameter is null in update method");
		
		
		Connection connexion = ConnexionMySQL.getInstance().getConnection();
		
		try
		{
			PreparedStatement updateDevise = connexion.prepareStatement("UPDATE Devise SET nom_dev = ? WHERE nom_dev = ?", PreparedStatement.RETURN_GENERATED_KEYS);
			updateDevise.setString(1, obj.getNom());
			updateDevise.setInt(2, obj.getId());
			updateDevise.executeUpdate();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			System.exit(1);
		}
	}

	@Override
	public void delete (Devise obj) throws RuntimeException
	{
		if (obj == null)
			throw new NullPointerException("Obj parameter is null in delete method");
		
		
		Connection connexion = ConnexionMySQL.getInstance().getConnection();
		
		try
		{
			PreparedStatement deleteDevise = connexion.prepareStatement("DELETE FROM Devise WHERE nom_dev = ?", PreparedStatement.RETURN_GENERATED_KEYS);
			deleteDevise.executeUpdate();
		}
		catch (SQLException e) 
		{
			int errorCode = e.getErrorCode();
			
			if (errorCode == 1451) // contraite de clé étrangère => devise encore utilisée
			{
				throw new RuntimeException("Devise encore utilisée : ne peut être supprimée");
			}
			
			e.printStackTrace();
			System.exit(1);
		}
	}

	@Override
	public Devise getByID (int id)
	{
		if (id <= 0)
			throw new NullPointerException("Obj parameter is null in delete method");
		
		
		Connection connexion = ConnexionMySQL.getInstance().getConnection();
		
		try
		{
			PreparedStatement getByID = connexion.prepareStatement("SELECT id_dev, nom_dev FROM Devise WHERE id_dev = ?");
			getByID.setInt(1, id);
			
			ResultSet devise = getByID.executeQuery();
			if (!devise.next())
				throw new SQLException("ResultSet de select dans MySQLDeviseDAO.getByID est vide");
			
			String nom_dev = devise.getString(2);
			return new Devise(nom_dev);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			System.exit(1);
		}
		
		return null;
	}
	
	@Override
	public Devise getByNom (String nom)
	{
		Connection connexion = ConnexionMySQL.getInstance().getConnection();
		
		try
		{
			Statement selectDevise = connexion.createStatement();
			ResultSet resultSelectDevise = selectDevise.executeQuery("SELECT TOP 1 * FROM Devise WHERE nom_dev = " + nom);
			
			if (!resultSelectDevise.next())
			{
				
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			System.exit(1);
		}

		return null;
	}

}
