package td3_dao.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map.Entry;

import td3_dao.dao.ContenuDAO;
import td3_dao.modele.metier.Devise;
import td3_dao.modele.metier.Portefeuille;

public class MySQLContenuDAO implements ContenuDAO
{
	private static MySQLContenuDAO instance;

	private MySQLContenuDAO() {}
	
	public static MySQLContenuDAO getInstance()
	{
		if (instance == null)
			instance = new MySQLContenuDAO();
		
		return instance;
	}
	
	@Override
	public void create (Portefeuille obj)
	{
		if (obj == null)
			throw new NullPointerException("Obj parameter is null in create method");
		
		
		Connection connexion = ConnexionMySQL.getInstance().getConnection();
		MySQLDeviseDAO deviseDAO = MySQLDeviseDAO.getInstance();
		
		try {
			PreparedStatement selectDeviseID = connexion.prepareStatement("SELECT id_dev FROM Devise WHERE nom_dev = ?");
			
			for (Devise devise : obj.getDevises())
			{
				int idDevise = -1;
				selectDeviseID.setString(1, devise.getNom());
				ResultSet resultSetDevise = selectDeviseID.executeQuery();
				
				if (resultSetDevise.next()) // la devise existe déjà
				{
					idDevise = resultSetDevise.getInt(1);
				}
				else
				{
					deviseDAO.create(devise);
					idDevise = devise.getId();
				}
				
				if (idDevise == -1)
					throw new SQLException("idDevise est incorrect (-1) dans MySQLContenuDAO.create");
				
				// Création de la ligne dans Contenu
				PreparedStatement insertContenu = connexion.prepareStatement("INSERT INTO Contenu VALUES (?, ?, ?)");
				insertContenu.setInt(1, obj.getId());
				insertContenu.setInt(2, idDevise);
				insertContenu.setDouble(3, obj.getSolde(devise.getNom()));
				
				insertContenu.executeUpdate();
			}
		}
		catch (SQLException excep) {
			System.err.println("Error : " + excep.getMessage());
			excep.printStackTrace();
			System.exit(1);
		}
	}

	@Override
	public void update (Portefeuille obj)
	{
		if (obj == null)
			throw new NullPointerException("Obj parameter is null in update method");
		
		
		Connection connexion = ConnexionMySQL.getInstance().getConnection();
		MySQLDeviseDAO deviseDAO = MySQLDeviseDAO.getInstance();
		
		for (Entry<Devise, Double> solde : obj.getSoldes().entrySet())
		{
			try
			{
				Statement selectDevise = connexion.createStatement();
				ResultSet resultSelectDevise = selectDevise.executeQuery("SELECT TOP 1 id_dev FROM Devise WHERE nom_dev = " + solde.getKey().getNom());
				if (!resultSelectDevise.next())
					deviseDAO.create(solde.getKey());
				
				PreparedStatement updateContenu = connexion.prepareStatement(
					"UPDATE Contenu c " +
					"INNER JOIN devise d ON c.id_dev = d.id_dev " +
					"INNER JOIN portefeuille p ON c.id_port = p.id_port " +
					"SET montant = ? " +
					"WHERE nom_dev = ? " +
					"AND nom_port = ?"
				);
				updateContenu.setDouble(1, solde.getValue()); // montant
				updateContenu.setString(2, solde.getKey().getNom()); // Nom devise
				updateContenu.setString(3, obj.getNom()); // Nom portefeuille
				
				updateContenu.executeUpdate();
			}
			catch (SQLException e) 
			{
				e.printStackTrace();
				System.exit(1);
			}
		}
	}

	@Override
	public void delete (Portefeuille obj)
	{
		// Suppression automatique dans la BDD => relation clé étrangère Contenu <=> Portefeuille en Cascade
	}
	
	@Override
	public HashMap<Devise, Double> getByPortefeuille (int id)
	{
		Connection connexion = ConnexionMySQL.getInstance().getConnection();
		HashMap<Devise, Double> soldes = null;
		
		try
		{
			PreparedStatement requete = connexion.prepareStatement(
				"SELECT nom_dev, montant " +
				"FROM devise d INNER JOIN contenu c ON d.id_dev = c.id_dev " +
				"WHERE id_port = ?"
			);
			
			requete.setInt(1, id);
			ResultSet result = requete.executeQuery();
			
			if (result.next()) {
				soldes = new HashMap<>();
				
				String nomDevise = result.getString(1);
				double solde = result.getDouble(2);
				
				soldes.put(new Devise(nomDevise), solde);
			}
			else
				return null;
			
			while (result.next()) {
				String nomDevise = result.getString(1);
				double solde = result.getDouble(2);
				
				soldes.put(new Devise(nomDevise), solde);
			}
			
			return soldes;
		}
		catch (SQLException excep)
		{
			System.out.println("Error : " + excep.getMessage());
		}
		
		return null;
	}
}
