package td3_dao.dao.mysql;

import java.io.FileInputStream;
import java.sql.*;
import java.util.Properties;

public class ConnexionMySQL
{
	private static ConnexionMySQL instance;
	
	private Connection connection;
	
	private ConnexionMySQL() { }
	
	public static ConnexionMySQL getInstance()
	{
		if (instance == null)
			instance = new ConnexionMySQL();
		 
		return instance;
	}
	
	public void connect()
	{
		boolean lectureFichierReussi;
		
		Properties prop = new Properties();
		try
		{
			FileInputStream fichier = new FileInputStream("configBDD.xml");
			prop.loadFromXML(fichier);
			lectureFichierReussi = true;
		}
		catch (Exception e)
		{
			System.out.println("Impossible de lire le fichier de configuration de la BdD.");
			lectureFichierReussi = false;
		}
		
		if (lectureFichierReussi)
		{
			String bdd   = prop.getProperty("jdbc.bdd");
			String host  = prop.getProperty("jdbc.host");
			String login = prop.getProperty("jdbc.login");
			String pwd   = prop.getProperty("jdbc.pwd");
			
			System.out.println("Connexion à " + bdd + " sur " + host + " en cours...");
			try
			{
				connection = DriverManager.getConnection("jdbc:mysql://" + host + "/" + bdd, login, pwd);
				System.out.println("- Connexion réussie -");
			}
			catch (SQLException sqle)
			{
				System.out.println("- Connexion impossible -\n" + sqle.getLocalizedMessage());
			}
		}
	}
	
	public void disconnect()
	{
		if (connection != null)
		{
			try
			{
				connection.close();
				System.out.println("- Déconnexion réussie -");
			}
			catch (SQLException sqle)
			{
				System.out.println("- Déconnexion impossible -");
			}
		}
	}
	
	public Connection getConnection()
	{
		boolean isClosed;
		
		if (connection == null) {
			isClosed = true;
		}
		else {
			try
			{
				isClosed = connection.isClosed();
			}
			catch (SQLException e)
			{
				isClosed = true;
			}
		}
		
		if (isClosed)
			connect();
		
		return connection;
	}
}