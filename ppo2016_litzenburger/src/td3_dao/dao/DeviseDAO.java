package td3_dao.dao;

import td3_dao.modele.metier.Devise;

public interface DeviseDAO extends DAO<Devise>
{
	public Devise getByID (int id);
	public Devise getByNom (String nom);
}
