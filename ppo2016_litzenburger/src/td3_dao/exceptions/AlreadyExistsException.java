package td3_dao.exceptions;

public class AlreadyExistsException extends RuntimeException
{
	private static final long serialVersionUID = 1L;
	private String message;
	
	public AlreadyExistsException (String message)
	{
		this.message = message;
	}
	
	@Override
	public String getMessage ()
	{
		return message;
	}
}