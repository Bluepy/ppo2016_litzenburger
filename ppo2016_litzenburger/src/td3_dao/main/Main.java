package td3_dao.main;

import td3_dao.dao.factory.DAOFactory;
import td3_dao.dao.factory.Persistance;
import td3_dao.exceptions.AlreadyExistsException;
import td3_dao.modele.metier.Devise;
import td3_dao.modele.metier.Portefeuille;

public class Main
{
	public static void main (String[] args)
	{
		Portefeuille p = new Portefeuille("Mon portefuille");
		
		p.ajoutDevise("Euros");
		p.creditSolde("Euros", 3500);
		p.ajoutDevise("Dollars");
		p.creditSolde("Dollars", 5);
		p.ajoutDevise("lol");
		p.creditSolde("Lol", 400);
		
		try
		{
			DAOFactory.getDAOFactory(Persistance.MYSQL).getPortefeuilleDAO().create(p);
		}
		catch (AlreadyExistsException e)
		{
			System.out.println(e.getMessage());
		}
	}
}