package td3_dao.modele.metier;

public class ClientPrivilégié extends Client
{
	private Privilège privilège;

	public ClientPrivilégié (String nom, String prenom, int ca)
	{
		super(nom, prenom, ca);

		if (ca >= Privilège.BON_CLIENT.getCaMin())
			privilège = Privilège.BON_CLIENT;
		else if (ca >= Privilège.CLIENT_EXCEPTIONNEL.getCaMin())
			privilège = Privilège.CLIENT_EXCEPTIONNEL;
		else if (ca >= Privilège.VIC.getCaMin())
			privilège = Privilège.VIC;
	}

	public ClientPrivilégié (Client c)
	{
		this(c.getNom(), c.getPrenom(), c.getCA());
	}

	@Override
	public String toString ()
	{
		return "[ClientPrivilégié]\n\tNom : " + this.getNom() +
				"\n\tPrénom : " + this.getPrenom() +
				"\n\tChiffre d'affaires : " + this.getCA() +
				"\n\tPrivilège : " + privilège.name() +
				"\n\tRéduction associée : " + privilège.getReduc() + "%";
	}

}
